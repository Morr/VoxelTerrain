using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VoxelTerrain;
using Utilities;
using MarchingCubesProject;

namespace VoxelTerrain
{
    [CustomEditor(typeof(MeshVoxelizer))]
    public class MeshVoxelizerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            TerrainChunkManager chunksManager = FindObjectOfType<TerrainChunkManager>();

            if (chunksManager != null)
            {
                if (GUILayout.Button("Voxelixe Mesh"))
                {
                    PerformBooleanOperation(chunksManager, false);
                }

                if (GUILayout.Button("Clear Mesh"))
                {
                    PerformBooleanOperation(chunksManager, true);
                }
            }
            else
            {
                GUILayout.Label($"Unable to find {nameof(TerrainChunkManager)} object");
            }

            serializedObject.ApplyModifiedProperties();
        }

        // TODO: Consider CellSize
        private void PerformBooleanOperation(TerrainChunkManager chunksManager, bool isReverse)
        {
            MeshVoxelizer meshVoxelizer = (MeshVoxelizer)target;
            MeshCloasestPointCalculator cloasestPointCalculator = new MeshCloasestPointCalculator(meshVoxelizer.Mesh, meshVoxelizer.transform.localToWorldMatrix);

            // Calculate operation bounds
            MeshCollider tmpMeshCollider = meshVoxelizer.gameObject.AddComponent<MeshCollider>();
            tmpMeshCollider.sharedMesh = meshVoxelizer.Mesh;
            Bounds bounds = tmpMeshCollider.bounds;

            Vector3Int margin = Vector3Int.one * Mathf.CeilToInt(1.0f / chunksManager.Settings.CellSize);
            Vector3Int min = chunksManager.CalculateWorldCellIndex(bounds.min) - margin;
            Vector3Int max = chunksManager.CalculateWorldCellIndex(bounds.max) + margin + Vector3Int.one;
            DestroyImmediate(tmpMeshCollider);

            HashSet<Vector3Int> dirtyChunksIndices = new();

            for (int x = min.x; x < max.x; x++)
            {
                for (int y = min.y; y < max.y; y++)
                {
                    for (int z = min.z; z < max.z; z++)
                    {
                        Vector3Int worldCellIndex = new Vector3Int(x, y, z);
                        Vector3 worldPosition = (Vector3)worldCellIndex * chunksManager.Settings.CellSize;

                        chunksManager.InverseTransformCellIndex(worldCellIndex, out Vector3Int chunkIndex, out Vector3Int localCellIndex);
                        TerrainChunk chunk;

                        if (chunksManager.TryGetChunk(chunkIndex, out chunk) == false)
                        {
                            chunksManager.TryDefineChunk(chunkIndex, out chunk);
                        }

                        if (chunk.IsEmpty == true)
                        {
                            chunk.InitializeEmptyVoxelArray();
                        }

                        // Calculate dirty chunks
                        dirtyChunksIndices.Add(chunkIndex);

                        // Calculate value
                        VoxelArray voxelData = chunk.GetVoxelData();
                        float currentValue = voxelData[localCellIndex.x, localCellIndex.y, localCellIndex.z];

                        Vector3 cloasestPointOnMesh = cloasestPointCalculator.GetCloasestPointOnMesh(worldPosition, out bool isInsideMesh);
                        float unsignedSurfaceDistance = Vector3.Distance(worldPosition, cloasestPointOnMesh);
                        float surfaceDistance = isInsideMesh == true ? unsignedSurfaceDistance : unsignedSurfaceDistance * -1.0f;

                        float newValue = (isReverse == true) ? Mathf.Min(currentValue, surfaceDistance * -1.0f) : Mathf.Max(currentValue, surfaceDistance);
                        voxelData[localCellIndex.x, localCellIndex.y, localCellIndex.z] = Mathf.Clamp(newValue, -1.0f, 1.0f);
                    }
                }
            }

            // TODO: improve filtering (now all modified chunks neighbours are set as dirty)
            foreach (Vector3Int chunkIndex in dirtyChunksIndices)
            {
                chunksManager.SetChunkDirty(chunkIndex);

                foreach (Vector3Int neighbourIndex in ArrayUtilities.GetNeighbourIndices(chunkIndex))
                {
                    if (dirtyChunksIndices.Contains(neighbourIndex) == false)
                    {
                        chunksManager.SetChunkDirty(neighbourIndex);
                    }
                }
            }
        }
    }
}
