using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace VoxelTerrain
{
    [CustomEditor(typeof(TerrainChunkManager))]
    public class TerrainChunkManagerEditor : Editor
    {
        private void OnEnable()
        {
        }
    }
}