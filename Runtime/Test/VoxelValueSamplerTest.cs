using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelTerrain;
using Utilities;

namespace VoxelTerrain
{
    public class VoxelValueSamplerTest : MonoBehaviour
    {
        #if UNITY_EDITOR
        [field: SerializeField]
        private TerrainChunkManager ChunksManager { get; set; }

        protected virtual void OnDrawGizmos()
        {
            Vector3Int cellIndex = transform.position.FloorToInt();
            Gizmos.color = Color.red;
            Gizmos.DrawSphere((Vector3)cellIndex * ChunksManager.Settings.CellSize, 0.05f);

            ChunksManager.InverseTransformCellIndex(cellIndex, out Vector3Int chunkIndex, out Vector3Int localIndex);

            if (ChunksManager.TryGetChunk(chunkIndex, out TerrainChunk chunk) == true)
            {
                if (chunk.IsEmpty == false)
                {
                    MarchingCubesProject.VoxelArray voxelData = chunk.GetVoxelData();
                    UnityEditor.Handles.Label(transform.position, voxelData[localIndex.x, localIndex.y, localIndex.z].ToString());
                }
            }
        }
        #endif
    }
}
