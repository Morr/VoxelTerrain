using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    public class CloasestMeshPointTest : MonoBehaviour
    {
        [field: SerializeField]
        private MeshFilter MeshFilter { get; set; }

        protected virtual void OnDrawGizmos()
        {
            if (MeshFilter == null)
            {
                return;
            }

            MeshCloasestPointCalculator calculator = new MeshCloasestPointCalculator(MeshFilter.sharedMesh, MeshFilter.transform.localToWorldMatrix);
            Vector3 cloasesMeshPoint = calculator.GetCloasestPointOnMesh(transform.position, out bool isInsideMesh);
            Gizmos.color = isInsideMesh == true ? Color.red : Color.yellow;
            Gizmos.DrawLine(transform.position, cloasesMeshPoint);
        }
    }
}
