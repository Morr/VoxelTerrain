using MarchingCubesProject;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    [DefaultExecutionOrder(10)]
    public class ChunkDataInitializerTest : MonoBehaviour
    {
        [field: SerializeField]
        private DataType InitDataType { get; set; }
        [field: SerializeField]
        private Vector3 SphereOffset { get; set; }
        [field: SerializeField]
        private TerrainChunkManager ChunksManager { get; set; }

        private TerrainChunk chunk;

        protected virtual void Awake()
        {
            Vector3Int chunkIndex = ChunksManager.CalculateChunkIndex(transform.position);

            if(ChunksManager.TryDefineChunk(chunkIndex, out chunk) == false)
            {
                ChunksManager.TryGetChunk(chunkIndex, out chunk);
            }

            InitializeChunkData();
            ChunksManager.SetChunkDirty(chunk);
        }

        private void InitializeChunkData()
        {
            chunk.InitializeEmptyVoxelArray();

            switch (InitDataType)
            {
                case DataType.Flat:
                    InitializeFlatData(chunk.GetVoxelData());
                    break;
                case DataType.Sphere:
                    InitializeSphereData(chunk.GetVoxelData());
                    break;
            }
        }

        public void InitializeFlatData(VoxelArray voxelData)
        {
            VoxelTerrainSettings settings = ChunksManager.Settings;
            float height = 5.0f;

            for (int x = 0; x < voxelData.Length[0]; x++)
            {
                for (int y = 0; y < voxelData.Length[1]; y++)
                {
                    for (int z = 0; z < voxelData.Length[2]; z++)
                    {
                        voxelData[x, y, z] = Mathf.Clamp(height - (y * settings.CellSize), -1.0f, 1.0f);
                    }
                }
            }
        }

        public void InitializeSphereData(VoxelArray voxelData)
        {
            VoxelTerrainSettings settings = ChunksManager.Settings;
            Vector3 sphereCenter = Vector3.one * settings.ChunkSize * 0.5f;
            sphereCenter += SphereOffset;
            float sphereRadius = 5.0f;

            for (int x = 0; x < voxelData.Length[0]; x++)
            {
                for (int y = 0; y < voxelData.Length[1]; y++)
                {
                    for (int z = 0; z < voxelData.Length[2]; z++)
                    {
                        Vector3 localPoint = new Vector3(x, y, z) * settings.CellSize;
                        voxelData[x, y, z] = Mathf.Clamp(sphereRadius - Vector3.Distance(sphereCenter, localPoint), -1.0f, 1.0f);
                    }
                }
            }
        }

        private enum DataType
        {
            Sphere,
            Flat
        }
    }
}

