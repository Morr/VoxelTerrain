using Common.Unity.Drawing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    public class TerrainChunkGizmos : MonoBehaviour
    {
        private NormalRenderer NormalRenderer { get; set; }
        private Mesh ChunkMesh { get; set; }

        protected virtual void Start()
        {
            ChunkMesh = GetComponent<MeshFilter>().mesh;
            InitializeNormalRenderer();
        }

        protected virtual void OnRenderObject()
        {
            DrawNormals();
        }

        private void InitializeNormalRenderer()
        {
            NormalRenderer = new NormalRenderer();
            NormalRenderer.DefaultColor = Color.red;
            NormalRenderer.Length = 0.25f;
            NormalRenderer.Load(ChunkMesh.vertices, ChunkMesh.normals);
        }

        private void DrawNormals()
        {
            NormalRenderer.LocalToWorld = transform.localToWorldMatrix;
            NormalRenderer.Draw();
        }
    }
}
