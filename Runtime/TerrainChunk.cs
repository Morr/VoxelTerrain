using UnityEngine;
using UnityEngine.Rendering;
using MarchingCubesProject;
using System.Collections.Generic;
using System;

namespace VoxelTerrain
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class TerrainChunk : MonoBehaviour
    {
        [field: SerializeField]
        private TerrainChunkManager ChunksManager { get; set; }
        [field: SerializeField]
        private Vector3Int ChunkIndex { get; set; }
        [field: SerializeField]
        private VoxelArray VoxelData { get; set; }

        [field: SerializeField]
        private VoxelDataContextCalculator ContextCalculator { get; set; }
        [field: SerializeField]
        private SetUVToWorld UVSetter { get; set; }

        private static List<Vector3> Vertices { get; set; } = new();
        private static List<Vector3> Normals { get; set; } = new();
        private static List<int> Indices { get; set; } = new();

        public bool IsEmpty => VoxelData.IsEmpty;
        private VoxelTerrainSettings Settings => ChunksManager.Settings;

        private MarchingCubes MarchingMethod { get; set; }
        private Mesh Mesh { get; set; }

        public VoxelArray GetVoxelData()
        {
            if (IsEmpty == true)
            {
                InitializeEmptyVoxelArray();
            }

            return VoxelData;
        }

        public void InitializeEmptyVoxelArray()
        {
            Vector3Int voxelArrayLength = new Vector3Int(Settings.ChunkArraySize, Settings.ChunkArraySize, Settings.ChunkArraySize);
            VoxelData = new VoxelArray(voxelArrayLength);

            for (int x = 0; x < VoxelData.Length[0]; x++)
            {
                for (int y = 0; y < VoxelData.Length[1]; y++)
                {
                    for (int z = 0; z < VoxelData.Length[2]; z++)
                    {
                        VoxelData[x, y, z] = Settings.MinValue;
                    }
                }
            }
        }

        public void InitializeChunk(Vector3Int chunkIndex, TerrainChunkManager chunksManager)
        {
            ChunkIndex = chunkIndex;
            ChunksManager = chunksManager;
        }

        public void RecalculateMesh()
        {
            if (Mesh == null)
            {
                InitializeMesh();
            }

            MarchingMethod = new();
            MarchingMethod.Surface = Settings.SurfaceValue;
            ContextCalculator.SetupContext(ChunksManager);

            if (IsEmpty == false)
            {
                ContextCalculator.CopyVoxelDataToContext(VoxelData);
                ContextCalculator.CopyVoxelDataFromNeighbourChunks(ChunkIndex, ChunksManager);
                MarchingMethod.GenerateMesh(ContextCalculator.ContextArray, Vertices, Indices, Vector3Int.one, Vector3Int.one);
            }
            else
            {
                ContextCalculator.ClearVoxelDataContext(Settings);
                ContextCalculator.CopyVoxelDataFromNeighbourChunks(ChunkIndex, ChunksManager);
                MarchingMethod.GenerateMesh(ContextCalculator.ContextArray, Vertices, Indices, ContextCalculator.ComplementaryIndices);
            }

            int vertexOffset = Vertices.Count;
            int indicesOffset = Indices.Count;
            MarchingMethod.GenerateMesh(ContextCalculator.ContextArray, Vertices, Indices, ContextCalculator.BorderIndices);

            // Move
            Vector3 translation = Vector3.one * -1.0f;
            MeshUtilities.TranslateVertices(Vertices, translation);

            // Scale
            if (Settings.CellSize != 1.0f)
            {
                MeshUtilities.ScaleVertices(Vertices, Settings.CellSize);
            }

            // Recalculate normals
            MeshUtilities.RecalculateNormals(Vertices, Indices, Normals);
            int verticesToRemove = Vertices.Count - vertexOffset;
            int indicesToRemove = Indices.Count - indicesOffset;

            Vertices.RemoveRange(vertexOffset, verticesToRemove);
            Normals.RemoveRange(vertexOffset, verticesToRemove);
            Indices.RemoveRange(indicesOffset, indicesToRemove);

            // Apply
            Mesh.Clear();
            Mesh.SetVertices(Vertices);
            Mesh.SetTriangles(Indices, 0);
            Mesh.SetNormals(Normals);
            Mesh.RecalculateBounds();
            UVSetter.RecalculateUVs(Mesh);

            Vertices.Clear();
            Normals.Clear();
            Indices.Clear();
        }

        private void InitializeMesh()
        {
            Mesh = new Mesh();
            GetComponent<MeshFilter>().sharedMesh = Mesh;
        }

        protected virtual void Start()
        {
            RecalculateMesh();
        }

        protected virtual void OnDestroy()
        {
            if (gameObject.scene.isLoaded == true)
            {
                ChunksManager.DestroyChunk(ChunkIndex, true);
            }
        }
    }
}

