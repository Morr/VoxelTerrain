using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    public class MeshVoxelizer : MonoBehaviour
    {
        [field: SerializeField]
        public Mesh Mesh { get; private set; }

        protected virtual void OnDrawGizmos()
        {
            if (Mesh != null)
            {
                Gizmos.color = new Color(1.0f, 1.0f, 1.0f, 0.1f);
                Gizmos.DrawMesh(Mesh, 0, transform.position, transform.rotation, transform.lossyScale);
            }
        }
    }
}
