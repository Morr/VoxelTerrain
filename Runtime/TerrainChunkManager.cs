using RotaryHeart.Lib.SerializableDictionary;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace VoxelTerrain
{
    [ExecuteInEditMode]
    public class TerrainChunkManager : MonoBehaviour
    {
        [field: SerializeField]
        public VoxelTerrainSettings Settings { get; private set; }
        [field: SerializeField]
        private GameObject ChunkPrefab { get; set; }
        [field: SerializeField]
        private SerializableDictionaryBase<Vector3Int, TerrainChunk> TerrainChunksMap { get; set; }

        private List<TerrainChunk> DirtyChunks { get; set; } = new();

        public void InverseTransformPoint(Vector3 position, out Vector3Int worldChunkIndex, out Vector3Int localCellIndex)
        {
            Vector3Int worldCellIndex = CalculateWorldCellIndex(position);
            InverseTransformCellIndex(worldCellIndex, out worldChunkIndex, out localCellIndex);
        }

        public void InverseTransformCellIndex(Vector3Int worldCellIndex, out Vector3Int chunkIndex, out Vector3Int localCellIndex)
        {
            Vector3 position = (Vector3)worldCellIndex * Settings.CellSize; // TODO: Optimize
            chunkIndex = CalculateChunkIndex(position);

            Vector3Int ChunkOriginIndex = CalculateChunkOriginCellIndex(chunkIndex);
            localCellIndex = worldCellIndex - ChunkOriginIndex;
        }

        public Vector3Int CalculateChunkIndex(Vector3 position)
        {
            return GetLoopedIndex(position, Settings.ChunkSize);
        }

        public bool TryGetChunk(Vector3Int chunkIndex, out TerrainChunk chunk)
        {
            return TerrainChunksMap.TryGetValue(chunkIndex, out chunk);
        }

        public bool TryDefineChunk(Vector3Int chunkIndex, out TerrainChunk chunkComponent)
        {
            if (TryGetChunk(chunkIndex, out TerrainChunk chunk) == false)
            {
                chunkComponent = InstantiateChunkObject(chunkIndex);
                TerrainChunksMap.Add(chunkIndex, chunkComponent);
                DefineEmptyNeighbours(chunkIndex);
                return true;
            }
            else
            {
                if (chunk.IsEmpty == true)
                {
                    DefineEmptyNeighbours(chunkIndex);
                }
            }

            chunkComponent = null;
            return false;
        }

        public void DestroyChunk(Vector3Int chunkIndex, bool isChunkObjectAlreadyDestroyed = false)
        {
            if (TerrainChunksMap.TryGetValue(chunkIndex, out TerrainChunk chunk) == true)
            {
                TerrainChunksMap.Remove(chunkIndex);

                if (isChunkObjectAlreadyDestroyed == false)
                {
                    DestroyImmediate(chunk.gameObject);
                }

                bool hasNonEmptyNeighbour = false;

                for (int i = 0; i < ArrayUtilities.NEIGHBOURS_OFFSETS.Length; i++)
                {
                    Vector3Int neighbourIndex = chunkIndex + ArrayUtilities.NEIGHBOURS_OFFSETS[i];

                    if (TryGetChunk(neighbourIndex, out TerrainChunk neighbourChunk) == true)
                    {
                        if (neighbourChunk.IsEmpty == false)
                        {
                            hasNonEmptyNeighbour = true;
                        }

                        DirtyChunks.Add(neighbourChunk);
                    }
                }

                if (hasNonEmptyNeighbour == true)
                {
                    DefineEmptyChunk(chunkIndex);
                }
            }
        }

        public void SetChunkDirty(TerrainChunk chunk)
        {
            if (DirtyChunks.Contains(chunk) == false) 
            {
                DirtyChunks.Add(chunk);
            }
        }

        public void SetChunkDirty(Vector3Int chunkIndex)
        {
            if (TerrainChunksMap.TryGetValue(chunkIndex, out TerrainChunk chunk))
            {
                SetChunkDirty(chunk);
            }
        }

        public Vector3Int CalculateWorldCellIndex(Vector3 position)
        {
            return GetLoopedIndex(position, Settings.CellSize);
        }

        protected virtual void Update()
        {
            RecalculateDirtyChunks();
        }

        private void RecalculateDirtyChunks()
        {
            for (int i = 0; i < DirtyChunks.Count; i++)
            {
                TerrainChunk chunk = DirtyChunks[i];

                if (chunk != null)
                {
                    chunk.RecalculateMesh();

                    #if UNITY_EDITOR
                    UnityEditor.EditorUtility.SetDirty(chunk);
                    #endif
                }
            }

            DirtyChunks.Clear();
        }

        private void DefineEmptyNeighbours(Vector3Int chunkIndex)
        {
            for (int i = 0; i < ArrayUtilities.PRECEDING_NEIGHBOURS_OFFSETS.Length; i++)
            {
                Vector3Int neighbourIndex = chunkIndex + ArrayUtilities.PRECEDING_NEIGHBOURS_OFFSETS[i];

                if (TerrainChunksMap.ContainsKey(neighbourIndex) == false)
                {
                    DefineEmptyChunk(neighbourIndex);
                }
            }
        }

        private void DefineEmptyChunk(Vector3Int chunkIndex)
        {
            TerrainChunk emptyChunk = InstantiateChunkObject(chunkIndex);
            TerrainChunksMap.Add(chunkIndex, emptyChunk);
            SetChunkDirty(emptyChunk);
        }

        private TerrainChunk InstantiateChunkObject(Vector3Int chunkIndex)
        {
            GameObject chunkObject = Instantiate(ChunkPrefab, transform);
            TerrainChunk chunkComponent = chunkObject.GetComponent<TerrainChunk>();
            chunkObject.name = chunkIndex.ToString();
            chunkObject.transform.position = (Vector3)chunkIndex * Settings.ChunkSize;
            chunkComponent.InitializeChunk(chunkIndex, this);
            return chunkComponent;
        }

        private Vector3Int GetLoopedIndex(Vector3 position, float size)
        {
            Vector3 loopedPosition = position / size;
            return loopedPosition.FloorToInt();
        }

        private Vector3Int CalculateChunkOriginCellIndex(Vector3Int chunkIndex)
        {
            return chunkIndex * Settings.ChunkArraySize;
        }

        #if UNITY_EDITOR
        private void OnValidate()
        {
            CleanupChunksDictionary();
        }
        #endif

        private void CleanupChunksDictionary()
        {
            List<Vector3Int> chunksKeysToRemove = new List<Vector3Int>();

            foreach (KeyValuePair<Vector3Int, TerrainChunk> chunkEntry in TerrainChunksMap)
            {
                if (chunkEntry.Value == null)
                {
                    chunksKeysToRemove.Add(chunkEntry.Key);
                }
            }

            for (int i = 0; i < chunksKeysToRemove.Count; i++)
            {
                TerrainChunksMap.Remove(chunksKeysToRemove[i]);
            }

            if (chunksKeysToRemove.Count > 0)
            {
                UnityEditor.EditorUtility.SetDirty(this);
            }
        }
    }
}
