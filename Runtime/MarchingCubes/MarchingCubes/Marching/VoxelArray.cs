using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchingCubesProject
{
    [System.Serializable]
    public class VoxelArray
    {
        [field: SerializeField]
        private float[] Values { get; set; } = null;
        [field: SerializeField]
        public Vector3Int Length { get; private set; }
        [field: SerializeField]
        private int LayerCellsCount { get; set; }

        public bool IsEmpty => Length == Vector3Int.zero;

        public float this[int x, int y, int z]
        {
            get { return Values[GetFlatIndex(x, y, z)]; }
            set { Values[GetFlatIndex(x, y, z)] = value; }
        }

        public VoxelArray(Vector3Int length)
        {
            Length = length;
            Values = new float[length[0] * length[1] * length[2]];
            LayerCellsCount = length[0] * length[2];
        }

        public int GetFlatIndex(int x, int y, int z)
        {
            return x + (z * Length[0]) + (y * LayerCellsCount);
        }
    }
}
