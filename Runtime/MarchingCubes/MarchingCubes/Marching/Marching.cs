using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace MarchingCubesProject
{
    public abstract class Marching
    {
        /// <summary>
        /// The surface value in the voxels. Normally set to 0. 
        /// </summary>
        public float Surface { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private float[] Cube { get; set; }

        /// <summary>
        /// Winding order of triangles use 2,1,0 or 0,1,2
        /// </summary>
        protected int[] WindingOrder { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surface"></param>
        public Marching(float surface)
        {
            Surface = surface;
            Cube = new float[8];
            WindingOrder = new int[] { 0, 1, 2 };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="voxels"></param>
        /// <param name="verts"></param>
        /// <param name="indices"></param>
        public virtual void GenerateMesh(VoxelArray voxels, IList<Vector3> verts, IList<int> indices, Vector3Int skipMin = default, Vector3Int skipMax = default)
        {
            UpdateWindingOrder();
            int x, y, z, i;
            int ix, iy, iz;

            int maxX = voxels.Length[0] - (skipMax.x + 1);
            int maxY = voxels.Length[1] - (skipMax.y + 1);
            int maxZ = voxels.Length[2] - (skipMax.z + 1);

            for (x = skipMin.x; x < maxX; x++)
            {
                for (y = skipMin.y; y < maxY; y++)
                {
                    for (z = skipMin.z; z < maxZ; z++)
                    {
                        //Get the values in the 8 neighbours which make up a cube
                        for (i = 0; i < 8; i++)
                        {
                            ix = x + VertexOffset[i, 0];
                            iy = y + VertexOffset[i, 1];
                            iz = z + VertexOffset[i, 2];

                            Cube[i] = voxels[ix, iy, iz];
                        }

                        //Perform algorithm
                        March(x, y, z, Cube, verts, indices);
                    }
                }
            }
        }

        public virtual void GenerateMesh(VoxelArray voxels, IList<Vector3> verts, IList<int> indices, IList<Vector3Int> voxelIndices)
        {
            UpdateWindingOrder();
            int i;
            int ix, iy, iz;

            for (int v = 0; v < voxelIndices.Count; v++)
            {
                Vector3Int voxelIndex = voxelIndices[v];

                for (i = 0; i < 8; i++)
                {
                    ix = voxelIndex.x + VertexOffset[i, 0];
                    iy = voxelIndex.y + VertexOffset[i, 1];
                    iz = voxelIndex.z + VertexOffset[i, 2];

                    Cube[i] = voxels[ix, iy, iz];
                }

                //Perform algorithm
                March(voxelIndex.x, voxelIndex.y, voxelIndex.z, Cube, verts, indices);
            }
        }

        /// <summary>
        /// Update the winding order. 
        /// This determines how the triangles in the mesh are orientated.
        /// </summary>
        protected virtual void UpdateWindingOrder()
        {
            if (Surface > 0.0f)
            {
                WindingOrder[0] = 2;
                WindingOrder[1] = 1;
                WindingOrder[2] = 0;
            }
            else
            {
                WindingOrder[0] = 0;
                WindingOrder[1] = 1;
                WindingOrder[2] = 2;
            }
        }

         /// <summary>
        /// MarchCube performs the Marching algorithm on a single cube
        /// </summary>
        protected abstract void March(float x, float y, float z, float[] cube, IList<Vector3> vertList, IList<int> indexList);

        /// <summary>
        /// GetOffset finds the approximate point of intersection of the surface
        /// between two points with the values v1 and v2
        /// </summary>
        protected virtual float GetOffset(float v1, float v2)
        {
            float delta = v2 - v1;
            return (delta == 0.0f) ? Surface : (Surface - v1) / delta;
        }

        /// <summary>
        /// VertexOffset lists the positions, relative to vertex0, 
        /// of each of the 8 vertices of a cube.
        /// vertexOffset[8][3]
        /// </summary>
        protected static readonly int[,] VertexOffset = new int[,]
	    {
	        {0, 0, 0},{1, 0, 0},{1, 1, 0},{0, 1, 0},
	        {0, 0, 1},{1, 0, 1},{1, 1, 1},{0, 1, 1}
	    };

    }

}
