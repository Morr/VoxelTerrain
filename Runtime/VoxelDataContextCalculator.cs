using MarchingCubesProject;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    [CreateAssetMenu(menuName = "VoxelTerrain/VoxelDataContextCalculator")]
    public class VoxelDataContextCalculator : ScriptableObject
    {
        [field: NonSerialized]
        public VoxelArray ContextArray { get; set; } = new (Vector3Int.zero);
        [field: NonSerialized]
        public List<Vector3Int> ComplementaryIndices { get; set; } = new(); // Complementary - partially defined by external chunk data
        [field: NonSerialized]
        public List<Vector3Int> BorderIndices { get; set; } = new(); // Used only for normal vectors calculation
        [field: NonSerialized]
        private CellCopyOperation[] CopyOperations { get; set; }

        public Vector3Int DataOffset { get; private set; } = Vector3Int.one;

        public void SetupContext(TerrainChunkManager chunksManager)
        {
            Vector3Int voxelArraySize = chunksManager.Settings.ChunkArraySizeVector;
            Vector3Int requestedContextLength = voxelArraySize + new Vector3Int(3, 3, 3);
            Vector3Int currentContextSize = ContextArray.Length;

            if (currentContextSize != requestedContextLength || CopyOperations == null)
            {
                InitializeContextArray(requestedContextLength);

                InitializeCopyOperations(chunksManager);
                RecalculateComplementaryIndices();
                RecalculateBorderIndices();
            }
        }

        public void CopyVoxelDataToContext(VoxelArray voxelArray)
        {
            for (int x = 0; x < voxelArray.Length[0]; x++)
            {
                for (int y = 0; y < voxelArray.Length[1]; y++)
                {
                    for (int z = 0; z < voxelArray.Length[2]; z++)
                    {
                        ContextArray[x + DataOffset.x, y + DataOffset.y, z + DataOffset.z] = voxelArray[x, y, z];
                    }
                }
            }
        }

        public void ClearVoxelDataContext(VoxelTerrainSettings settings)
        {
            for (int x = 0; x < ContextArray.Length[0]; x++)
            {
                for (int y = 0; y < ContextArray.Length[1]; y++)
                {
                    for (int z = 0; z < ContextArray.Length[2]; z++)
                    {
                        ContextArray[x, y, z] = settings.MinValue;
                    }
                }
            }
        }

        public void CopyVoxelDataFromNeighbourChunks(Vector3Int chunkIndex, TerrainChunkManager chunksManager)
        {
            Vector3Int lastChunkIndexOffset = Vector3Int.zero;
            TerrainChunk otherChunk;
            VoxelArray otherChunkVoxelData = null;

            for (int i = 0; i < CopyOperations.Length; i++)
            {
                CellCopyOperation copyOperation = CopyOperations[i];
                Vector3Int sourceChunkOffset = copyOperation.SourceChunkIndexOffset;

                if (copyOperation.SourceChunkIndexOffset != lastChunkIndexOffset)
                {
                    if (chunksManager.TryGetChunk(chunkIndex + sourceChunkOffset, out otherChunk) == false)
                    {
                        otherChunk = null;
                    }

                    if (otherChunk != null && otherChunk.IsEmpty == false)
                    {
                        otherChunkVoxelData = otherChunk.GetVoxelData();
                    }
                    else
                    {
                        otherChunkVoxelData = null;
                    }

                    lastChunkIndexOffset = copyOperation.SourceChunkIndexOffset;
                }

                float value = chunksManager.Settings.MinValue;

                if (otherChunkVoxelData != null)
                {
                    value = otherChunkVoxelData[copyOperation.SourceLocalCellIndex.x, copyOperation.SourceLocalCellIndex.y, copyOperation.SourceLocalCellIndex.z];
                }

                ContextArray[copyOperation.ContextArrayIndex.x, copyOperation.ContextArrayIndex.y, copyOperation.ContextArrayIndex.z] = value;
            }
        }

        private void InitializeContextArray(Vector3Int contextArrayLength)
        {
            ContextArray = new VoxelArray(contextArrayLength);
        }

        private void InitializeCopyOperations(TerrainChunkManager chunksManager)
        {
            List<CellCopyOperation> copyOperationsList = new();

            for (int x = 0; x < ContextArray.Length[0]; x++)
            {
                for (int y = 0; y < ContextArray.Length[1]; y++)
                {
                    for (int z = 0; z < ContextArray.Length[2]; z++)
                    {
                        Vector3Int contextArrayIndex = new Vector3Int(x, y, z);
                        Vector3Int worldIndex = contextArrayIndex - DataOffset;

                        if (IsValueDefinedInOtherChunk(contextArrayIndex) == true)
                        {
                            chunksManager.InverseTransformCellIndex(worldIndex, out Vector3Int chunkIndex, out Vector3Int localCellIndex);
                            CellCopyOperation copyOperation = new CellCopyOperation(chunkIndex, localCellIndex, contextArrayIndex);
                            copyOperationsList.Add(copyOperation);
                        }
                    }
                }
            }

            copyOperationsList.Sort();
            CopyOperations = copyOperationsList.ToArray();

            bool IsValueDefinedInOtherChunk(Vector3Int localIndex)
            {
                for (int i = 0; i < 3; i++)
                {
                    int index = localIndex[i];

                    if (index == 0 || index >= ContextArray.Length[i] - 2)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private void RecalculateComplementaryIndices()
        {
            ComplementaryIndices.Clear();
            Vector3Int complementartyCoordinates = ContextArray.Length - new Vector3Int(3, 3, 3);

            for (int x = DataOffset.x; x <= complementartyCoordinates.x; x++)
            {
                for (int y = DataOffset.y; y <= complementartyCoordinates.y; y++)
                {
                    for (int z = DataOffset.z; z <= complementartyCoordinates.z; z++)
                    {
                        if (x == complementartyCoordinates.x || y == complementartyCoordinates.y || z == complementartyCoordinates.z)
                        {
                            ComplementaryIndices.Add(new Vector3Int(x, y, z));
                        }
                    }
                }
            }
        }

        private void RecalculateBorderIndices()
        {
            BorderIndices.Clear();
            Vector3Int borderMax = ContextArray.Length - new Vector3Int(2, 2, 2);

            for (int x = 0; x <= borderMax.x; x++)
            {
                for (int y = 0; y <= borderMax.y; y++)
                {
                    for (int z = 0; z <= borderMax.z; z++)
                    {
                        Vector3Int index = new Vector3Int(x, y, z);

                        for (int i = 0; i < 3; i++)
                        {
                            if (index[i] == 0 || index[i] == borderMax[i])
                            {
                                BorderIndices.Add(index);
                                break;
                            }
                        }
                    }
                }
            }
        }

        private struct CellCopyOperation : IComparable<CellCopyOperation>
        {
            public Vector3Int SourceChunkIndexOffset;
            public Vector3Int SourceLocalCellIndex;
            public Vector3Int ContextArrayIndex;

            public CellCopyOperation (Vector3Int sourceChunkIndexOffset, Vector3Int sourceLocalIndex, Vector3Int contextArrayIndex)
            {
                SourceChunkIndexOffset = sourceChunkIndexOffset;
                SourceLocalCellIndex = sourceLocalIndex;
                ContextArrayIndex = contextArrayIndex;
            }

            public int CompareTo(CellCopyOperation other)
            {
                int xCompare = SourceChunkIndexOffset.x.CompareTo(other.SourceChunkIndexOffset.x);

                if (xCompare == 0)
                {
                    int yCompare = SourceChunkIndexOffset.y.CompareTo(other.SourceChunkIndexOffset.y);

                    if (yCompare == 0)
                    {
                        return SourceChunkIndexOffset.z.CompareTo(other.SourceChunkIndexOffset.z);
                    }

                    return yCompare;
                }

                return xCompare;
            }

            public override string ToString()
            {
                return $"ChunkOffset: {SourceChunkIndexOffset}, LocalCellIndex: {SourceLocalCellIndex} -> ContextArrayIndex: {ContextArrayIndex}";
            }
        }
    }
}
