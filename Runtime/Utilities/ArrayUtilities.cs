using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    public static class ArrayUtilities
    {
        public static readonly Vector3Int[] NEIGHBOURS_OFFSETS;
        public static readonly Vector3Int[] PRECEDING_NEIGHBOURS_OFFSETS;

        static ArrayUtilities()
        {
            List<Vector3Int> neighboursOffsets = new();
            List<Vector3Int> precedingNeighboursOffsets = new();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    for (int z = -1; z <= 1; z++)
                    {
                        Vector3Int offset = new Vector3Int(x, y, z);

                        if (offset != Vector3Int.zero)
                        {
                            neighboursOffsets.Add(offset);

                            if (x < 1 && y < 1 && z < 1)
                            {
                                precedingNeighboursOffsets.Add(offset);
                            }
                        }
                    }
                }
            }

            NEIGHBOURS_OFFSETS = neighboursOffsets.ToArray();
            PRECEDING_NEIGHBOURS_OFFSETS = precedingNeighboursOffsets.ToArray();
        }

        public static Vector3Int GetLengthVector<T>(this T[,,] array)
        {
            return new Vector3Int(array.GetLength(0), array.GetLength(1), array.GetLength(2));
        }

        public static IEnumerable GetNeighbourIndices(Vector3Int index)
        {
            for (int i = 0; i < NEIGHBOURS_OFFSETS.Length; i++)
            {
                yield return index + NEIGHBOURS_OFFSETS[i];
            }
        }
    }

}
