using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    public static class MeshUtilities
    {
        public static void TranslateVertices(List<Vector3> vertices, Vector3 translation)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i] += translation;
            }
        }

        public static void ScaleVertices(List<Vector3> vertices, float scale)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i] *= scale;
            }
        }

        public static void RecalculateNormals(IList<Vector3> vertices, IList<int> indices, IList<Vector3> normals)
        {
            normals.Clear();
            Dictionary<Vector3, Vector3> accumulatedNormals = new Dictionary<Vector3, Vector3>();

            for (int i = 0; i < indices.Count; i += 3)
            {
                int i1 = indices[i];
                int i2 = indices[i + 1];
                int i3 = indices[i + 2];

                Vector3 v1 = vertices[i1];
                Vector3 v2 = vertices[i2];
                Vector3 v3 = vertices[i3];

                Vector3 firstEdge = v2 - v1;
                Vector3 secondEdge = v3 - v1;
                Vector3 triangleNormal = Vector3.Cross(firstEdge, secondEdge).normalized; // Should be weighted

                AccumulateNormal(v1, v2, v3);

                void AccumulateNormal(params Vector3[] vertices)
                {
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        Vector3 vertex = vertices[i];

                        if (accumulatedNormals.TryAdd(vertex, triangleNormal) == false)
                        {
                            accumulatedNormals[vertex] += triangleNormal;
                        }
                    }
                }
            }

            for (int i = 0; i < vertices.Count; i++)
            {
                normals.Add(accumulatedNormals[vertices[i]].normalized);
            }
        }
    }
}
