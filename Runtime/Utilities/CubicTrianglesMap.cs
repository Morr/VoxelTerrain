using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace VoxelTerrain
{
    public class CubicTrianglesMap
    {
        private static float RAY_SAMPLE_INTERVAL = 0.5f;

        private Dictionary<Vector3Int, List<int>> CubicMap { get; set; } = new();

        public CubicTrianglesMap(IList<Vector3> vertices, IList<int> indices)
        {
            HashTriangles(vertices, indices);
        }

        private void HashTriangles(IList<Vector3> vertices, IList<int> indices)
        {
            int triangleIndex = 0;

            while (triangleIndex < indices.Count)
            {
                int i1 = indices[triangleIndex];
                int i2 = indices[triangleIndex + 1];
                int i3 = indices[triangleIndex + 2];

                Vector3 v1 = vertices[i1];
                Vector3 v2 = vertices[i2];
                Vector3 v3 = vertices[i3];

                // Triangle vertices
                RegisterPoint(v1);
                RegisterPoint(v2);
                RegisterPoint(v3);

                // Triangle edges
                RegisterSection(v1, v2);
                RegisterSection(v2, v3);
                RegisterSection(v3, v1);

                triangleIndex += 3;
            }

            void RegisterSection(Vector3 begin, Vector3 end)
            {
                Vector3 sectionVector = end - begin;
                Ray ray = new Ray(begin, sectionVector);
                float sectionLength = sectionVector.magnitude;
                float distance = RAY_SAMPLE_INTERVAL;

                while (distance < sectionLength)
                {
                    RegisterPoint(ray.GetPoint(distance));
                    distance += RAY_SAMPLE_INTERVAL;
                }
            }

            void RegisterPoint(Vector3 point)
            {
                Vector3Int cubeIndex = point.FloorToInt();

                if (CubicMap.TryGetValue(cubeIndex, out List<int> list) == false)
                {
                    list = new List<int>();
                    CubicMap.Add(cubeIndex, list);
                }

                if (list.Contains(triangleIndex) == false)
                {
                    list.Add(triangleIndex);
                }
            }
        }
    }
}

