using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace VoxelTerrain
{
    public class MeshCloasestPointCalculator
    {
        private Vector3[] Vertices { get; set; }
        private int[] Indices { get; set; }
        private CubicTrianglesMap CubicTrianglesMap { get; set; }

        public MeshCloasestPointCalculator(Mesh mesh, Matrix4x4 localToWorldMatrix)
        {
            Vertices = new Vector3[mesh.vertices.Length];

            for (int i = 0; i < Vertices.Length; i++)
            {
                Vertices[i] = localToWorldMatrix.MultiplyPoint(mesh.vertices[i]);
            }

            Indices = mesh.triangles;
            CubicTrianglesMap = new CubicTrianglesMap(Vertices, Indices);
        }

        public Vector3 GetCloasestPointOnMesh(Vector3 point, out bool isInsideMesh)
        {
            float minSqrDistance = float.PositiveInfinity;
            Vector3 cloasestPoint = point;
            List<Vector3> closestTrianglesVertices = new();

            for (int i = 0; i < Indices.Length; i += 3)
            {
                int i1 = Indices[i];
                int i2 = Indices[i + 1];
                int i3 = Indices[i + 2];
                Vector3 v1 = Vertices[i1];
                Vector3 v2 = Vertices[i2];
                Vector3 v3 = Vertices[i3];

                Vector3 cloasestPointOnTriangle = GetClosestPointTriangle(point, v1, v2, v3);
                float sqrDistance = Vector3.SqrMagnitude(cloasestPointOnTriangle - point);

                if (sqrDistance < minSqrDistance)
                {
                    closestTrianglesVertices.Clear();
                    closestTrianglesVertices.Add(v1);
                    closestTrianglesVertices.Add(v2);
                    closestTrianglesVertices.Add(v3);

                    minSqrDistance = sqrDistance;
                    cloasestPoint = cloasestPointOnTriangle;
                }
                else if (sqrDistance == minSqrDistance && cloasestPoint == cloasestPointOnTriangle)
                {
                    closestTrianglesVertices.Add(v1);
                    closestTrianglesVertices.Add(v2);
                    closestTrianglesVertices.Add(v3);
                }
            }


            if (closestTrianglesVertices.Count > 3)
            {
                Vector3 vertexNormal = Vector3.zero;

                for (int i = 0; i < closestTrianglesVertices.Count; i += 3)
                {
                    Vector3 v1 = closestTrianglesVertices[i];
                    Vector3 v2 = closestTrianglesVertices[i + 1];
                    Vector3 v3 = closestTrianglesVertices[i + 2];

                    Vector3 triangleNormal = Vector3.Cross(v2 - v1, v3 - v1).normalized;
                    vertexNormal += triangleNormal;
                }

                for (int i = 0; i < closestTrianglesVertices.Count; i += 3)
                {

                }

                Vector3 normal = point - cloasestPoint;
                float angle = Vector3.Angle(normal, vertexNormal);
                isInsideMesh = angle > 90.0f;
            }
            else
            {
                Plane cloasestTrianglePlane = new Plane(closestTrianglesVertices[0], closestTrianglesVertices[1], closestTrianglesVertices[2]);
                isInsideMesh = !cloasestTrianglePlane.GetSide(point);
            }

            return cloasestPoint;
        }

        public static Vector3 GetClosestPointTriangle(Vector3 p, Vector3 a, Vector3 b, Vector3 c)
        {
            Vector3 ab = b - a;
            Vector3 ac = c - a;
            Vector3 ap = p - a;

            float d1 = Vector3.Dot(ab, ap);
            float d2 = Vector3.Dot(ac, ap);
            if (d1 <= 0.0f && d2 <= 0.0f) return a; //#1

            Vector3 bp = p - b;
            float d3 = Vector3.Dot(ab, bp);
            float d4 = Vector3.Dot(ac, bp);
            if (d3 >= 0.0f && d4 <= d3) return b; //#2

            Vector3 cp = p - c;
            float d5 = Vector3.Dot(ab, cp);
            float d6 = Vector3.Dot(ac, cp);
            if (d6 >= 0.0f && d5 <= d6) return c; //#3

            float vc = d1 * d4 - d3 * d2;
            if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f)
            {
                float v1 = d1 / (d1 - d3);
                return a + v1 * ab; //#4
            }

            float vb = d5 * d2 - d1 * d6;
            if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f)
            {
                float v1 = d2 / (d2 - d6);
                return a + v1 * ac; //#5
            }

            float va = d3 * d6 - d5 * d4;
            if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f)
            {
                float v1 = (d4 - d3) / ((d4 - d3) + (d5 - d6));
                return b + v1 * (c - b); //#6
            }

            float denom = 1.0f / (va + vb + vc);
            float v = vb * denom;
            float w = vc * denom;
            return a + v * ab + w * ac; //#0
        }
    }
}
