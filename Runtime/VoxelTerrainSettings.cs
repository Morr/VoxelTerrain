using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrain
{
    [CreateAssetMenu(menuName = "VoxelTerrain/VoxelTerrainSettings")]
    public class VoxelTerrainSettings : ScriptableObject
    {
        [field: SerializeField]
        public int ChunkArraySize { get; private set; }
        [field: SerializeField]
        public float CellSize { get; private set; }
        [field: SerializeField]
        public float SurfaceValue { get; private set; }

        public float MinValue { get; private set; } = -1.0f;
        public float ChunkSize => ChunkArraySize * CellSize;
        public Vector3Int ChunkArraySizeVector => new Vector3Int(ChunkArraySize, ChunkArraySize, ChunkArraySize);
    }
}
